// jQuery
// @codekit-prepend "../_dev/bower_components/jquery/dist/jquery.min.js"

// noUiSlider
// @codekit-prepend "../_dev/bower_components/wnumb/wNumb.js"
// @codekit-prepend "../_dev/bower_components/nouislider/distribute/nouislider.min.js"

// Slick carousel
// @codekit-prepend "../_dev/bower_components/slick-carousel/slick/slick.min.js"

// Plyr
// @codekit-prepend "../_dev/bower_components/plyr/dist/plyr.js";

// Owl carousel
// @codekit-prepend "../_dev/bower_components/owl.carousel/dist/owl.carousel.min.js";

// jQuery Nice Select
// @codekit-prepend "../_dev/bower_components/jquery-nice-select/js/jquery.nice-select.min.js";
