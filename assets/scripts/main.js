// @codekit-prepend "_plugins.js"

$(document).ready(function() {

	// Mobile nav
	$('#siteNav')
		.clone()
		.appendTo('.c-page-header')
		.addClass('c-site-nav--mobile js-mobileNav');

	$('#menuToggleBtn').on('click', function() {
		$('.js-mobileNav').toggleClass('is-open');
	});




	// Range slider
	var rangeSliderPrice = document.getElementById('rangeSliderPrice');

	if ($(rangeSliderPrice).length) {

    noUiSlider.create(rangeSliderPrice, {
		cssPrefix: 'c-noUi-',
		connect: true,
		format: wNumb({
			decimals: $(rangeSliderPrice).data('slider-decimals'),
			prefix: '$'
		}),
		range: {
			'min': $(rangeSliderPrice).data('slider-min'),
			'max': $(rangeSliderPrice).data('slider-max')
		},
		start: [$(rangeSliderPrice).data('slider-start'), $(rangeSliderPrice).data('slider-stop')],
		step: $(rangeSliderPrice).data('slider-step'),
		tooltips: true
	    });
	}




	// Owl carousel
	var $carousel = $('div.js-carousel'),
		$owlContainerWidth = $carousel.parent().width(),
		$slidesOffset = Math.round(($owlContainerWidth * 16.243654822) / 100);

	$carousel.owlCarousel({
		autoplay: true,
		autoplaySpeed: 300,
		autoplayTimeout: 4000,
		autoplayHoverPause: true,
		center: true,
		dots: false,
		items: 3,
		loop: true,
		margin: -$slidesOffset,
		slideSpeed: 300,
		mouseDrag: false,
		nav: true,
		navSpeed: 300
	});




	// Plyr media player
	var playBtnLarge = [
		'<button class="plyr__play-large" type="button" data-plyr="play" aria-label="play">',
			'<i class="c-icon c-icon--play"></i>',
		'</button>'
	].join('');

	plyr.setup({
		controls: ['play-large'],
		html: playBtnLarge
	});




	// jQuery Nice Select
	var $select = $('select.js-select'),
		$query = $('#menuToggleBtn'),
		$isVisible = $query.is(':visible');

	if (!$isVisible) {
		$select.niceSelect();
	}

});
